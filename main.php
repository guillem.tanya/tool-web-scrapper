<?php
use Symfony\Component\Panther\Client;
use Symfony\Component\DomCrawler\Crawler;

require __DIR__.'/vendor/autoload.php'; // Composer's autoloader

define("OUT_DIR", "out");
$host_es = "https://www.houzz.es";
$limit = false; #Number of iteration for path (useful in development)
$sections = [
    "es_architects" => "/professionals/arquitectos",
    "es_architects_tech" => "/professionals/arquitectos-tecnicos-y-aparejadores"
];

global $client;
$client = Client::createFirefoxClient();


println("STARTING TO SCRAP HOUZZ");


$current_section = "es_architects_tech";
println("Current section: $current_section");

//Check if there's an stopped old process
if( !($next = @file_get_contents(OUT_DIR."/$current_section.next")) ){
    $next = $sections[$current_section];
    writeCsvHeaders($current_section); // Open and init CSV File
}

$counter = 0;
$fp = fopen(OUT_DIR."/$current_section.csv", 'a');

do{
    println("Next path: $next");
    $crawler = getHtmlPageCrawler($host_es.$next);
    $json = getHouzzJsonData($crawler);

    $professionals = [];
    $userData = $json->data->stores->data->UserStore->data;
    $professionalData = $json->data->stores->data->ProfessionalStore->data;
    $viewProfessionalData = $json->data->stores->data->ViewProfessionalsStore->data->userIdsToProEventInfos;

    echo "Professionals recieved in iteration $counter are " . count((array)$userData) ."\n";

    foreach ($professionalData as $jsonProf){

        if(property_exists($viewProfessionalData, $jsonProf->userId)) $view = $viewProfessionalData->{$jsonProf->userId};
        else $view = null;
        $p = getProfessionalFromJson(
            $jsonProf,
            $userData->{$jsonProf->userId},
            $view
        );

        $professionals[$p->userId] = $p;
        fputcsv($fp,get_object_vars($p));

    }

    $last = $next;
    $next = getNextPathLink($crawler);
    file_put_contents(OUT_DIR."/$current_section.last",$last);
    file_put_contents(OUT_DIR."/$current_section.next",$next);
    $counter++;
    sleep(rand(1,10));
}while((!$limit || $counter<$limit) && $next);


fclose($fp);



/**
 * @param $page path
 * @return Crawler
 */
function  getHtmlPageCrawler($page){
    global $client;

    $client->request('GET', $page);
//    $crawler = $client->waitForVisibility('.hz-visitor-consents');
//    $client->executeScript("document.getElementsByClassName('hz-take-consents__success-button')[0].click()");
    $client->waitFor('.hz-pro-search-results__item');
    $crawler = $client->refreshCrawler();
    return new Crawler($crawler->html());
}

/**
 * @param $crawler
 * @return JSON houzz data code
 */
function getHouzzJsonData($crawler){
//    file_put_contents("tech.json", $crawler->filterXPath('//script[@id="hz-ctx"]')->html());
//    exit();
    return json_decode($crawler->filterXPath('//script[@id="hz-ctx"]')->html());
}

/**
 * @param $crawler
 * @return false if no next page in pagination exists
 * @return string with next page link path
 */
function getNextPathLink($crawler){

    $nextObj = $crawler->filterXPath('//a[contains(@class,"hz-pagination-link--next")]');
    if( $nextObj->count() > 0 ){
        return $nextLink = $nextObj->first()->attr("href");
    }else{
        return false;
    }
}


/**
 * @param $professional json Houzz data structure
 * @param $user json Houzz data structure
 * @param $view json Houzz data structure
 * @return stdClass object with filtered professional data
 */
function getProfessionalFromJson($professional, $user, $view){
    $p = new stdClass();
    $p->userId = $professional->userId;
    $p->professionalId = $professional->professionalId;
    $p->formattedPhone = $professional->formattedPhone;
    $p->professionalName = html_entity_decode( $user->displayName );
    $p->contactName = html_entity_decode($user->contactName );
    $p->firstName = html_entity_decode($user->firstName );
    $p->username = html_entity_decode($user->userName );
    $p->name = $professional->proTypeDisplayName;
    $p->country = html_entity_decode ($professional->country);
    $p->state = html_entity_decode ($professional->state);
    $p->postcode = html_entity_decode ($professional->zip);
    $p->city = html_entity_decode ($professional->city);
    $p->location = html_entity_decode ($professional->location);
    $p->address = html_entity_decode ($professional->address);

    if($view) {
        $link = $view->profileClk;
        $p->profileLink = "$link->protocol://$link->domain$link->path";
    }else{
        $p->profileLink = "";
    }

    $p->reviewRating = $professional->reviewRating;
    $p->numReviews = $professional->numReviews;
    $p->aboutMe = html_entity_decode ($professional->aboutMe);

    return $p;
}

function writeCsvHeaders($current_section){
    $fp = fopen(OUT_DIR."/$current_section.csv", 'w');
    $headers = array("userId","professionalId","formattedPhone","professionalName","contactName","firstName","username","name","country","state","postcode","city","location","address","profileLink","reviewRating","numReviews","aboutMe");
    fputcsv($fp,$headers);
    fclose($fp);
}

function println($message){
    echo $message."\n";
}